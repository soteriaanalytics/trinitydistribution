
#import <Foundation/Foundation.h>

@interface TrinityTimeMetricsHelper : NSObject

+ (TrinityTimeMetricsHelper*) sharedInstance;

-(void) recordStartTimeForFlow: (NSString*) flowName;
-(void) recordEndTimeForFlow: (NSString*) flowName;

@end
