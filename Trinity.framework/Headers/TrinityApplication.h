

#import <UIKit/UIKit.h>

@protocol TrinityApplicationEventDelegate;


@interface TrinityApplication : UIApplication

+ (id)sharedApplication NS_EXTENSION_UNAVAILABLE_IOS("Not available in extensions");

@property (nonatomic, weak) id <TrinityApplicationEventDelegate> eventDelegate;

@end


@protocol TrinityApplicationEventDelegate <NSObject>

@required

-(void) applicationDidReceivedUserInput:(TrinityApplication *) application;

@end
