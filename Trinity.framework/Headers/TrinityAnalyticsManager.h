

#import <Foundation/Foundation.h>

typedef enum {
    SAL_APP_LAUNCH
} SoteriaCustomEventsType;

static NSString* SAL_CUSTOM_EVENT_APP_LAUNCH=@"appLaunch";

@interface TrinityAnalyticsManager : NSObject

+ (void) initializeTrinityWithAccessToken:(NSString*) accessToken;

    
//Shared singleton instance for the Analytics Manager
//A new instance is created the first time this is called
+ (TrinityAnalyticsManager*) sharedInstance;

+(void) setCslEndPoint:(NSString*) cslEndPoint;
+(void) enableLogging:(BOOL) enableLogging;

//Use this method to log Custom errors
//Will be recorded as TrinityError events in the logs
-(void) logTrinityError:(NSString*) message;

//Use this method to log Custom errors
//Will be recorded as TrinityError events in the logs
-(void) logTrinityEvent:(NSString*) message;

//Use this method to log Custom network events
//Will be recorded as NetworkEvent events in the logs
-(void) logNetworkEvent:(NSString*) message;

//Use this method to log Custom UI events
//Will be recorded as UIEvent events in the logs
-(void) logUIEvent:(NSString*) message;


//This method is used to post the app crash message to the server.
//To be used with Soteria Crash Reporter
-(void) recordAppCrash:(NSString*) message;

//This method pushes the logs to the server
//The trigger value should be a string indicating what triggered the logs to be pushed.
//Sample values for trigger are - Logoff, Background, SessionTimeOut etc
- (void)  postLoggingToServer: (NSString*) trigger;


//This method retrieves the unique device id created by Soteria
+ (NSString*) getDeviceID;

//Capture App Launch Time
-(void) setAppLaunchTime: (NSString*) appLaunchTime;

//Captutre Network Connectivity Type
- (NSString*)getNetworkConnectionType;

@end
