

#import <UIKit/UIKit.h>


@interface TrinityBaseViewController : UIViewController

//Custom Screen Name for the View Controller
//This name will be captured in the logs
@property (nonatomic, retain) NSString* screenName;

@end


