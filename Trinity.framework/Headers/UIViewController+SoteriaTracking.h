//
//  UIViewController+SoteriaTracking.h
//  Trinity
//
//  Created by Navdeep Mahajan on 4/3/19.
//  Copyright © 2019 Sucheta Bhandare. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (SoteriaTracking)

@end

NS_ASSUME_NONNULL_END
